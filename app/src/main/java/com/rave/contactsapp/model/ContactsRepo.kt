package com.rave.contactsapp.model

import com.rave.contactsapp.model.local.ContactEntity
import com.rave.contactsapp.model.local.ContactsDatabase
import javax.inject.Inject

class ContactsRepo @Inject constructor(
    val contactsDatabase: ContactsDatabase
) {
    private val contactsDao = contactsDatabase.contactsDao()

    suspend fun getContacts(): List<ContactEntity> {
        return contactsDao.getContacts()
    }

    suspend fun addContact(contact: ContactEntity) {
        return contactsDao.addContact(contact)
    }

    suspend fun editContact(contact: ContactEntity) {
        return contactsDao.updateContact(contact)
    }
}