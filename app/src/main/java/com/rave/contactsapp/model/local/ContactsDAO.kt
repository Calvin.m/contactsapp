package com.rave.contactsapp.model.local

import androidx.room.*

@Dao
interface ContactsDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addContact(contact: ContactEntity)

    @Query("SELECT * FROM contact_table")
    suspend fun getContacts(): List<ContactEntity>

    @Update
    suspend fun updateContact(contact: ContactEntity)

    @Delete
    suspend fun deleteContact(contact: ContactEntity)
}