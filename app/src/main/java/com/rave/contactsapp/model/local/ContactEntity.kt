package com.rave.contactsapp.model.local

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contact_table")
data class ContactEntity(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    @ColumnInfo(name = "first_name") val firstName: String,
    @ColumnInfo(name = "last_name") val lastName: String,
    @Embedded val address: Address?,
    val email: String?,
    val phoneNumber: String?
)

data class Address(
    val street: String?,
    val state: String?,
    val city: String?,
    @ColumnInfo(name = "zip_code") val zipCode: Int?
)
