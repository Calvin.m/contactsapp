package com.rave.contactsapp.model.di

import androidx.room.Room
import com.rave.contactsapp.ContactsApplication
import com.rave.contactsapp.model.local.ContactsDAO
import com.rave.contactsapp.model.local.ContactsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    fun providesContactDatabase(): ContactsDatabase {
        return Room.databaseBuilder(
            ContactsApplication.applicationContext(),
            ContactsDatabase::class.java,
            "contactsDatabase"
        ).build()
    }
}