package com.rave.contactsapp.viewmodel.editcontact

import com.rave.contactsapp.model.local.ContactEntity

data class EditContactState(
    val contact: ContactEntity
)
