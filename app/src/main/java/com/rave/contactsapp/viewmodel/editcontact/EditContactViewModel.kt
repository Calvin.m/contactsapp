package com.rave.contactsapp.viewmodel.editcontact

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.contactsapp.model.ContactsRepo
import com.rave.contactsapp.model.local.ContactEntity
import com.rave.contactsapp.viewmodel.contactslist.ContactsListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EditContactViewModel @Inject constructor(val repo: ContactsRepo) : ViewModel() {

    private val _viewState = MutableStateFlow(ContactsListState())
    val viewState get() = _viewState.asStateFlow()

    fun editContact(contact: ContactEntity) {
        viewModelScope.launch {
            repo.editContact(contact)
        }
    }

}