package com.rave.contactsapp.viewmodel.addcontact

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.contactsapp.model.ContactsRepo
import com.rave.contactsapp.model.local.ContactEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddContactViewModel @Inject constructor(val repo: ContactsRepo) : ViewModel() {

    private val _viewState = MutableStateFlow(AddContactState())
    val viewState get() = _viewState.asStateFlow()

    fun updateFirstName(name: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(firstName = name) }
        }
    }

    fun updateLastName(name: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(lastName = name) }
        }
    }

    fun updateStreetNumber(streetNumber: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(streetNumber = streetNumber) }
        }
    }

    fun updateCityName(name: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(cityName = name) }
        }
    }

    fun updateStateName(name: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(stateName = name) }
        }
    }

    fun updateZipCode(name: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(zipCode = name) }
        }
    }

    fun updateEmail(name: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(email = name) }
        }
    }

    fun updatePhoneNumber(name: String) {
        viewModelScope.launch {
            _viewState.update { it.copy(phoneNumber = name) }
        }
    }

    fun addContact(contact: ContactEntity) {
        viewModelScope.launch {
            _viewState.update { it.copy(isLoading = true) }
            repo.addContact(contact)
            _viewState.update { it.copy(isLoading = false) }
        }
    }
}