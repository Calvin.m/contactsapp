package com.rave.contactsapp.viewmodel.contactslist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.contactsapp.model.ContactsRepo
import com.rave.contactsapp.viewmodel.addcontact.AddContactState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContactsListViewModel @Inject constructor(val repo: ContactsRepo) : ViewModel() {

    private val _viewState = MutableStateFlow(ContactsListState())
    val viewState get() = _viewState.asStateFlow()

    fun getContacts() {
        viewModelScope.launch {
            _viewState.update { it.copy(isLoading = true) }
            val results = repo.getContacts()
            _viewState.update { it.copy(isLoading = false, contactsList = results) }
        }
    }

//    fun getContactDetails(id: Int) {
//
//
//
//    }
}