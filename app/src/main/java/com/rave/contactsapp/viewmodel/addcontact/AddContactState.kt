package com.rave.contactsapp.viewmodel.addcontact

data class AddContactState(
    val isLoading: Boolean = false,
    val id: Int = 0,
    val firstName: String = "",
    val lastName: String = "",
    val streetNumber: String = "",
    val cityName: String = "",
    val stateName: String = "",
    val zipCode: String = "",
    val email: String = "",
    val phoneNumber: String = ""
)
