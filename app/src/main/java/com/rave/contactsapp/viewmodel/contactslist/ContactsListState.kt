package com.rave.contactsapp.viewmodel.contactslist

import com.rave.contactsapp.model.local.ContactEntity

data class ContactsListState(
    val isLoading: Boolean = false,
    val contactsList: List<ContactEntity> = emptyList()
)
