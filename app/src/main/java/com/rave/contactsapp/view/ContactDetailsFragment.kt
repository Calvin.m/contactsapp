package com.rave.contactsapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.rave.contactsapp.R
import com.rave.contactsapp.model.local.ContactEntity
import com.rave.contactsapp.viewmodel.addcontact.AddContactViewModel
import com.rave.contactsapp.viewmodel.contactslist.ContactsListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContactDetailsFragment : Fragment() {
    private val contactsListViewModel by activityViewModels<ContactsListViewModel>()
    private val addContactViewModel by activityViewModels<AddContactViewModel>()
    private val contactId by lazy { arguments?.getString("contactId") }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                contactsListViewModel.getContacts()
                val state by contactsListViewModel.viewState.collectAsState()
                if (contactId == null) {
                    CircularProgressIndicator()
                }
                Column() {
                    val contact = state.contactsList.find {
                        contactId?.toInt() == it.id

                    }
                    Text("Details Screen")
                    Button(onClick = { findNavController().navigate(R.id.contactsListFragment) }) {
                        Text("Home Screen")
                    }
                    Card(
                        Modifier
                            .fillMaxWidth()
                            .padding(12.dp)
                    ) {
                        Column(
                            Modifier.fillMaxWidth(),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Text(
                                ("First Name: " + contact?.firstName ?: ""),
                                textAlign = TextAlign.Center
                            )
                            Text(
                                ("Last Name: " + contact?.lastName ?: ""),
                                textAlign = TextAlign.Center
                            )
                            Text(
                                ("Address ${contact?.address?.street}"),
                                textAlign = TextAlign.Center
                            )
                            Text(
                                ("Address ${contact?.address?.city}"),
                                textAlign = TextAlign.Center
                            )
                            Text(
                                ("Address ${contact?.address?.state}"),
                                textAlign = TextAlign.Center
                            )
                            Text(
                                ("Address ${contact?.address?.zipCode}"),
                                textAlign = TextAlign.Center
                            )
                            Text(
                                ("Address ${contact?.email}"),
                                textAlign = TextAlign.Center
                            )
                            Text(
                                ("Address ${contact?.phoneNumber}"),
                                textAlign = TextAlign.Center
                            )
                            Button(onClick = {
                                addContactViewModel.updateFirstName(contact?.firstName ?: "")
                                addContactViewModel.updateLastName(contact?.lastName ?: "")
                                addContactViewModel.updateStreetNumber(contact?.address?.street ?: "")
                                addContactViewModel.updateCityName(contact?.address?.city ?: "")
                                addContactViewModel.updateStateName(contact?.address?.state ?: "")
                                addContactViewModel.updateZipCode(contact?.address?.zipCode.toString() ?: "")
                                addContactViewModel.updateEmail(contact?.email ?: "")
                                addContactViewModel.updatePhoneNumber(contact?.phoneNumber ?: "")
                                val args = bundleOf("contactId" to contactId)
                                findNavController().navigate(
                                    resId = R.id.contactEditFragment,
                                    args = args
                                )
                            }) {
                                Text("Edit")
                            }
                        }
                    }
                }
            }
        }
    }
}