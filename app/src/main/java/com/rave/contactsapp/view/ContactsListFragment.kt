package com.rave.contactsapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.rave.contactsapp.R
import com.rave.contactsapp.viewmodel.contactslist.ContactsListViewModel
import dagger.hilt.android.AndroidEntryPoint
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Delete
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.core.os.bundleOf

@AndroidEntryPoint
class ContactsListFragment : Fragment() {
    private val contactsListViewModel by viewModels<ContactsListViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                contactsListViewModel.getContacts()
                val state by contactsListViewModel.viewState.collectAsState()
                Column() {
                    Text("Contacts List Fragment!")
                    Button(onClick = {
                        findNavController().navigate(R.id.addContactFragment)
                    }) {
                        Text("Add Contact")
                    }
                    LazyColumn(modifier = Modifier.padding(12.dp)) {
                        items(state.contactsList) { item ->
                            val args = bundleOf("contactId" to item.id.toString())
                            Card(
                                modifier = Modifier
                                    .clickable {
                                        findNavController().navigate(
                                            resId = R.id.contactDetailsFragment,
                                            args = args
                                        )
                                    }
                                    .padding(12.dp),
                                elevation = CardDefaults.cardElevation(8.dp),
                            ) {
                                Row(
                                    modifier = Modifier
                                        .padding(12.dp)
                                        .fillMaxWidth()
                                ) {
                                    Column(
                                        Modifier
                                            .weight(1f)
                                            .fillMaxSize(),
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        verticalArrangement = Arrangement.Center
                                    ) {
                                        Text("${item.id}")
                                    }
                                    Column(
                                        Modifier
                                            .weight(3f)
                                            .fillMaxSize(),
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        verticalArrangement = Arrangement.Center
                                    ) {

                                        Text(item.firstName + " " + item.lastName)
//                                        Text(item.lastName)
                                    }
                                    Column(Modifier.weight(1f)) {
                                        Button(
                                            onClick = {},
                                            colors = ButtonDefaults.buttonColors(Color.Transparent)
                                        ) {
                                            Icon(Icons.Rounded.Delete, contentDescription = null)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}