package com.rave.contactsapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.*
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.rave.contactsapp.R
import com.rave.contactsapp.model.local.Address
import com.rave.contactsapp.model.local.ContactEntity
import com.rave.contactsapp.viewmodel.addcontact.AddContactViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@AndroidEntryPoint
class AddContactFragment : Fragment() {
    private val addContactViewModel by viewModels<AddContactViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by addContactViewModel.viewState.collectAsState()
                Column() {
                    Text("Add Contact Page")
                    TextField(
                        placeholder = { Text("First Name") },
                        value = state.firstName,
                        onValueChange = {
                            addContactViewModel.updateFirstName(it)
                        })
                    TextField(
                        placeholder = { Text("Second Name") },
                        value = state.lastName,
                        onValueChange = {
                            addContactViewModel.updateLastName(it)
                        })
                    TextField(
                        placeholder = { Text("Street Number") },
                        value = state.streetNumber,
                        onValueChange = {
                            addContactViewModel.updateStreetNumber(it)
                        })
                    TextField(
                        placeholder = { Text("City Name") },
                        value = state.cityName,
                        onValueChange = {
                            addContactViewModel.updateCityName(it)
                        })
                    TextField(
                        placeholder = { Text("State") },
                        value = state.stateName,
                        onValueChange = {
                            addContactViewModel.updateStateName(it)
                        })
                    TextField(
                        placeholder = { Text("Zip Code") },
                        value = state.zipCode,
                        onValueChange = {
                            addContactViewModel.updateZipCode(it)
                        })
                    TextField(
                        placeholder = { Text("Email") },
                        value = state.email,
                        onValueChange = {
                            addContactViewModel.updateEmail(it)
                        })
                    TextField(
                        placeholder = { Text("Phone Number") },
                        value = state.phoneNumber,
                        onValueChange = {
                            addContactViewModel.updatePhoneNumber(it)
                        })
                    Button(onClick = {
                        val contact = ContactEntity(
                            null,
                            state.firstName,
                            state.lastName,
                            address = Address(
                                state.streetNumber,
                                state.cityName,
                                state.stateName,
                                state.zipCode.toInt()
                            ),
                            email = state.email,
                            phoneNumber = state.phoneNumber
                        )
                        addContactViewModel.addContact(contact)
                        if (!state.isLoading) {
                            findNavController().navigate(R.id.contactsListFragment)
                        }
                    }) {
                        Text("Save Contact")
                    }
                }
            }
        }
    }
}