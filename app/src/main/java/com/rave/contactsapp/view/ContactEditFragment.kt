package com.rave.contactsapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.*
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.rave.contactsapp.R
import com.rave.contactsapp.model.local.Address
import com.rave.contactsapp.model.local.ContactEntity
import com.rave.contactsapp.viewmodel.addcontact.AddContactViewModel
import com.rave.contactsapp.viewmodel.contactslist.ContactsListViewModel
import com.rave.contactsapp.viewmodel.editcontact.EditContactViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContactEditFragment : Fragment() {
    private val contactsListViewModel by activityViewModels<ContactsListViewModel>()
    private val addContactViewModel by activityViewModels<AddContactViewModel>()
    private val contactId by lazy { arguments?.getString("contactId") }
    private val editContactViewModel by activityViewModels<EditContactViewModel>()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by contactsListViewModel.viewState.collectAsState()
                val addContactState by addContactViewModel.viewState.collectAsState()
                Column {
                    val contact = state.contactsList.find {
                        contactId?.toInt() == it.id
                    }
                    if (contactId == null) {
                        CircularProgressIndicator()
                    } else {
                        Text("Edit Screen")
                        OutlinedTextField(value = addContactState.firstName, onValueChange = {
                            addContactViewModel.updateFirstName(it)
                        })
                        OutlinedTextField(value = addContactState.lastName, onValueChange = {
                            addContactViewModel.updateLastName(it)
                        })
                        OutlinedTextField(value = addContactState.streetNumber, onValueChange = {
                            addContactViewModel.updateStreetNumber(it)
                        })
                        OutlinedTextField(value = addContactState.cityName, onValueChange = {
                            addContactViewModel.updateCityName(it)
                        })
                        OutlinedTextField(value = addContactState.stateName, onValueChange = {
                            addContactViewModel.updateStateName(it)
                        })
                        OutlinedTextField(value = addContactState.zipCode, onValueChange = {
                            addContactViewModel.updateZipCode(it)
                        })
                        OutlinedTextField(value = addContactState.email, onValueChange = {
                            addContactViewModel.updateEmail(it)
                        })
                        OutlinedTextField(value = addContactState.phoneNumber, onValueChange = {
                            addContactViewModel.updatePhoneNumber(it)
                        })
                        Button(onClick = {
                            val contact =
                                ContactEntity(
                                    contact?.id,
                                    addContactState.firstName,
                                    addContactState.lastName,
                                    address = Address(
                                        street = addContactState.streetNumber,
                                        state = addContactState.stateName,
                                        city = addContactState.cityName,
                                        zipCode = addContactState.zipCode.toInt()
                                    ),
                                    email = addContactState.email,
                                    phoneNumber = addContactState.phoneNumber
                                )
                            editContactViewModel.editContact(contact)
                            findNavController().navigate(
                                resId = R.id.contactDetailsFragment,
                                args = bundleOf("contactId" to contactId)
                            )

                        }) {
                            Text("Save Changes")
                        }
                    }

                }
            }
        }
    }
}